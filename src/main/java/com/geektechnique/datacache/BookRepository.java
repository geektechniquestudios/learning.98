package com.geektechnique.datacache;

public interface BookRepository {

    Book getByIsbn(String isbn);

}