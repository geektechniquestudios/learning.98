package com.geektechnique.datacache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class DataCacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataCacheApplication.class, args);
    }

}
